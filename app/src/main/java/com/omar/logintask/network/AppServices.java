package com.omar.logintask.network;

import com.omar.logintask.Routs;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AppServices {

    private static final String Base_URL = "http://api.mediastack.com/v1/";
    private static Retrofit instance;
    private static Routs routs;

    public static Retrofit getInestance() {
        if (instance == null) {
            instance = new Retrofit.Builder().baseUrl(Base_URL).
                    addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        return instance;
    }


    public  static Routs routs(){
        if  (routs ==null){
            routs=getInestance().create(Routs.class);
        }
        return routs;
    }


}

