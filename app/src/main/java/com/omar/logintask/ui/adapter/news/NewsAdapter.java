package com.omar.logintask.ui.adapter.news;

import android.service.autofill.OnClickAction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.omar.logintask.R;
import com.omar.logintask.model.Data;
import com.omar.logintask.model.ModelNews;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.MyViewHolder> {
    List<Data> newsModelArrayList;
    OnClickItem onClickItem;
    OnClickItemSendTittle  onClickItemSendTittle;


    public interface OnClickItem {
        public void newsItemClick(Data data,int  position);
    }

    public interface OnClickItemSendTittle {
        public void onClickItemSendTittle(String tittle);
    }

    public NewsAdapter(OnClickItem onClickItem,OnClickItemSendTittle onClickItemSendTittle,List<Data> newsModelArrayList) {
        this.newsModelArrayList = newsModelArrayList;
        this.onClickItem=onClickItem;
        this.onClickItemSendTittle=onClickItemSendTittle;

    }

    // for  connect with  item row
    @NonNull
    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_item_news, parent, false);
        return new MyViewHolder(itemView);
    }


    // for  connect data  with View in row  item
    @Override
    public void onBindViewHolder(@NonNull @NotNull MyViewHolder holder, int position) {
        if (newsModelArrayList != null) {


            holder.tvTitle.setText(newsModelArrayList.get(position).author);
//            holder.tvTDesc.setText(newsModelArrayList.get(position).getDescription());
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickItem.newsItemClick(newsModelArrayList.get(position),position);
            }
        });


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickItemSendTittle.onClickItemSendTittle(newsModelArrayList.get(position).getTitle());
            }
        });



    }


    //for  list size
    @Override
    public int getItemCount() {
        if (newsModelArrayList != null)
            return newsModelArrayList.size();
        else
            return 0;

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle;
        TextView tvTDesc;
        ImageView imgNews;

        public MyViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);

            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvTDesc = itemView.findViewById(R.id.tvDescreption);
            imgNews= itemView.findViewById(R.id.imgNews);

        }
    }
}
