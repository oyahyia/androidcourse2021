package com.omar.logintask.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;
import com.omar.logintask.R;
import com.omar.logintask.base.BaseFregment;
import com.omar.logintask.model.Data;
import com.omar.logintask.model.ModelNews;
import com.omar.logintask.ui.adapter.news.NewsAdapter;
import com.omar.logintask.viewmodel.news.NewsViewModel;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class NewsFragment extends BaseFregment {


    private NewsAdapter newsAdapter = null;
    ArrayList<ModelNews> newsModelArrayList = new ArrayList<>();

    RecyclerView recNews;

    private NewsViewModel newsViewModel;

    ProgressBar progressBar;

    @Override
    public void onAttach(@NonNull @NotNull Context context) {
        super.onAttach(context);
        newsViewModel= ViewModelProviders.of(this).get(NewsViewModel.class);
    }

    @Override
    public void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ShowProgress(progressBar);
        newsViewModel.getAllNews().observe(this, new Observer<List<Data>>() {
            @Override
            public void onChanged(List<Data> data) {
                hideProgress(progressBar);
                newsAdapter = new NewsAdapter(new NewsAdapter.OnClickItem() {
                    @Override
                    public void newsItemClick(Data data1, int position) {
                        Bundle bundle = new Bundle();
                        bundle.putParcelable("NewsDetails", data1);
                        openFragmentWithBundle(new NewsFragmentDetails(), "NewsFragmentDetails", bundle);
                        // make toast
                        Toast.makeText(requireContext(), "MMMMM", Toast.LENGTH_LONG).show();



                    }
                }, new NewsAdapter.OnClickItemSendTittle() {
                    @Override
                    public void onClickItemSendTittle(String tittle) {

                    }
                }, data);
                recNews.setAdapter(newsAdapter);
            }


        });

    }


    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.news_fragment, container, false);
        recNews = rootView.findViewById(R.id.recNews);
        progressBar=rootView.findViewById(R.id.pBNews);

        Toast.makeText(requireContext(),"Text for test Git ", Toast.LENGTH_LONG).show();

        return rootView;
    }


    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
