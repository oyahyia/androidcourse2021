package com.omar.logintask.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class Root implements Parcelable {
    public Pagination pagination;
    public List<Data> data;

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.pagination, flags);
        dest.writeTypedList(this.data);
    }

    public void readFromParcel(Parcel source) {
        this.pagination = source.readParcelable(Pagination.class.getClassLoader());
        this.data = source.createTypedArrayList(Data.CREATOR);
    }

    public Root() {
    }

    protected Root(Parcel in) {
        this.pagination = in.readParcelable(Pagination.class.getClassLoader());
        this.data = in.createTypedArrayList(Data.CREATOR);
    }

    public static final Parcelable.Creator<Root> CREATOR = new Parcelable.Creator<Root>() {
        @Override
        public Root createFromParcel(Parcel source) {
            return new Root(source);
        }

        @Override
        public Root[] newArray(int size) {
            return new Root[size];
        }
    };
}
