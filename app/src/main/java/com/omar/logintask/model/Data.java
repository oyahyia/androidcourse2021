package com.omar.logintask.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity
public class Data implements Parcelable {
    @PrimaryKey(autoGenerate = true)
    public int  Id;
    @SerializedName("author")
    public String author;
    @SerializedName("title")
    public String title;
    @SerializedName("description")
    public String description;
    @SerializedName("url")
    public String urlNews;
    @SerializedName("source")
    public String source;
    @SerializedName("image")
    public String imageNews;
    @SerializedName("category")
    public String category;
    @SerializedName("language")
    public String language;
    @SerializedName("country")
    public String country;
    @SerializedName("published_at")
    public String published_at;

    public String getUrlNews() {
        return urlNews;
    }

    public void setUrlNews(String urlNews) {
        this.urlNews = urlNews;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageNews() {
        return imageNews;
    }

    public void setImageNews(String imageNews) {
        this.imageNews = imageNews;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPublished_at() {
        return published_at;
    }

    public void setPublished_at(String published_at) {
        this.published_at = published_at;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.author);
        dest.writeString(this.title);
        dest.writeString(this.description);
        dest.writeString(this.urlNews);
        dest.writeString(this.source);
        dest.writeString(this.imageNews);
        dest.writeString(this.category);
        dest.writeString(this.language);
        dest.writeString(this.country);
        dest.writeString(this.published_at);
    }

    public void readFromParcel(Parcel source) {
        this.author = source.readString();
        this.title = source.readString();
        this.description = source.readString();
        this.urlNews = source.readString();
        this.source = source.readString();
        this.imageNews = source.readString();
        this.category = source.readString();
        this.language = source.readString();
        this.country = source.readString();
        this.published_at = source.readString();
    }

    public Data() {
    }

    protected Data(Parcel in) {
        this.author = in.readString();
        this.title = in.readString();
        this.description = in.readString();
        this.urlNews = in.readString();
        this.source = in.readString();
        this.imageNews = in.readString();
        this.category = in.readString();
        this.language = in.readString();
        this.country = in.readString();
        this.published_at = in.readString();
    }

    public static final Parcelable.Creator<Data> CREATOR = new Parcelable.Creator<Data>() {
        @Override
        public Data createFromParcel(Parcel source) {
            return new Data(source);
        }

        @Override
        public Data[] newArray(int size) {
            return new Data[size];
        }
    };
}
