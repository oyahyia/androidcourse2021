package com.omar.logintask.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Pagination implements Parcelable {

    public int limit;
    public int offset;
    public int count;
    public int total;

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.limit);
        dest.writeInt(this.offset);
        dest.writeInt(this.count);
        dest.writeInt(this.total);
    }

    public void readFromParcel(Parcel source) {
        this.limit = source.readInt();
        this.offset = source.readInt();
        this.count = source.readInt();
        this.total = source.readInt();
    }

    public Pagination() {
    }

    protected Pagination(Parcel in) {
        this.limit = in.readInt();
        this.offset = in.readInt();
        this.count = in.readInt();
        this.total = in.readInt();
    }

    public static final Parcelable.Creator<Pagination> CREATOR = new Parcelable.Creator<Pagination>() {
        @Override
        public Pagination createFromParcel(Parcel source) {
            return new Pagination(source);
        }

        @Override
        public Pagination[] newArray(int size) {
            return new Pagination[size];
        }
    };
}
