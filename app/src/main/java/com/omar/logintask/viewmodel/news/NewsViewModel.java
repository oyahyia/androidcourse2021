package com.omar.logintask.viewmodel.news;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.omar.logintask.dao.NewsDao;
import com.omar.logintask.model.Data;
import com.omar.logintask.model.ModelNews;
import com.omar.logintask.model.Root;
import com.omar.logintask.network.AppServices;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewsViewModel extends AndroidViewModel {
    public NewsViewModel(@NonNull @NotNull Application application) {
        super(application);
    }


    private NewsDao newsDao;
    List<Data> modelNewsArrayList = new ArrayList<>();


    public MutableLiveData<List<Data>> mutableLiveData = null;

    public MutableLiveData<List<Data>> getAllNews() {
        if (mutableLiveData == null) {
            mutableLiveData = new MutableLiveData<>();
            getNewsData();
        }
        return mutableLiveData;
    }


    public void getNewsData() {
        AppServices.routs().getAllPublicNews().enqueue(new Callback<Data>() {
            @Override
            public void onResponse(Call<Data> call, Response<Data> response) {
                modelNewsArrayList.add(response.body());
//              newsDao.insert(modelNewsArrayList);
                //send data to fragment
                mutableLiveData.setValue(modelNewsArrayList);



            }

            @Override
            public void onFailure(Call<Data> call, Throwable t) {

            }
        });
    }


}
