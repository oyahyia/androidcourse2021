package com.omar.logintask.base;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import com.omar.logintask.R;
import com.omar.logintask.ui.fragment.NewsFragment;
//black
public class BaseActivity extends AppCompatActivity {


    FragmentManager fm;
    static Activity activity;
    void GetView(){
        activity = BaseActivity.this;
        fm = ((BaseActivity) activity).getSupportFragmentManager();

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        GetView();


        //gren
        openFragmentWithBundle(new NewsFragment(),"NewsFragment",null);

    }


    public void openFragmentWithBundle(Fragment fragment, String tag, Bundle bundle) {
        FragmentTransaction ft;
        fragment.setArguments(bundle);
                                  //red
        ft = fm.beginTransaction().replace(R.id.news_contenar, fragment, tag);
        ft.addToBackStack(tag);
        ft.commit();
    }
}