package com.omar.logintask.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.omar.logintask.model.Data;

import java.util.List;

@Dao
public interface NewsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<Data> data);

    @Query("Select * from Data where language =:lang")
    Data getDataByType(String lang);


}
