package com.omar.logintask.loginpage;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.omar.logintask.R;
import com.omar.logintask.base.BaseActivity;

public class LoginpageActivity extends AppCompatActivity {
    Button logininbtn;
    Button regbtn;
    EditText edtname,edtpassword;
    TextView forgotPassword;

    void GetView(){
        logininbtn=findViewById(R.id.logininbtn);
        edtname =findViewById(R.id.edtname);
        edtpassword=findViewById(R.id.edtpassword);
        forgotPassword=findViewById(R.id.txvforgotPassword);
        regbtn=findViewById(R.id.regbtn);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loginpage);
        GetView();
        logininbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(LoginpageActivity.this, BaseActivity.class);
                startActivity(intent);
                finish();

            }
        });

        regbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(LoginpageActivity.this, SignUpActivity.class);
                startActivity(intent);
            }
        });

    }
}