package com.omar.logintask.loginpage;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.omar.logintask.R;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {

    EditText edtemail;
    EditText edtpassword;
    Button signupbtn;

    void GetView() {
        edtemail = findViewById(R.id.edtemail);
        edtpassword = findViewById(R.id.edtpassword);
        signupbtn = findViewById(R.id.signupbtn);
    }

    public static final String MY_PREFS_NAME;

    static {
        MY_PREFS_NAME = "MyPrefsFile";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        GetView();


        signupbtn.setOnClickListener(this);


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.signupbtn:
                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                editor.clear();
                editor.putString("email", edtemail.getText().toString());
                editor.putString("password", edtpassword.getText().toString());
                editor.apply();
                Toast.makeText(SignUpActivity.this, "Saved", Toast.LENGTH_LONG).show();

                break;


        }
    }
}