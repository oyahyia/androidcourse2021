package com.omar.logintask.dbs;


import android.content.Context;

import androidx.room.Database;
import androidx.room.Entity;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.omar.logintask.dao.NewsDao;
import com.omar.logintask.model.Data;

@Database(entities = {Data.class}, version = 1, exportSchema = false)
public abstract class NewsDBS  extends RoomDatabase {


    public  abstract NewsDao newsDao();

    private static NewsDBS ourInestance;

    public static NewsDBS getDBS(final Context context){
    if  (ourInestance ==  null){
        ourInestance= Room.databaseBuilder(context.getApplicationContext(),
                NewsDBS.class ,"NewsDBS").allowMainThreadQueries().build();
    }

    return ourInestance;

    }

}
